# ci-badges

A few Redox CI badges in one place

| Project | Status |
|---------|--------|
| Acid | [![Build status](https://gitlab.redox-os.org/redox-os/acid/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/acid/pipelines) |
| OrbTk | [![Build status](https://gitlab.redox-os.org/redox-os/orbtk/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/orbtk/pipelines) |
| RedoxOS | [![Build status](https://gitlab.redox-os.org/redox-os/redox/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/redox/pipelines) |
| Ion | [![Build status](https://gitlab.redox-os.org/redox-os/ion/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/ion/pipelines) |
| ion-plugins | [![Build status](https://gitlab.redox-os.org/redox-os/ion-plugins/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/ion-plugins/pipelines) |
| posix-regex | [![Build status](https://gitlab.redox-os.org/redox-os/posix-regex/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/posix-regex/pipelines) |
| syscall | [![Build status](https://gitlab.redox-os.org/redox-os/syscall/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/syscall/pipelines) |
| rusttype | [![Build status](https://gitlab.redox-os.org/redox-os/rusttype/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/rusttype/pipelines) |
| relibc | [![Build status](https://gitlab.redox-os.org/redox-os/relibc/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/relibc/pipelines) |
| orbital | [![Build status](https://gitlab.redox-os.org/redox-os/orbital/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/orbital/pipelines) |
| termion | [![Build status](https://gitlab.redox-os.org/redox-os/termion/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/termion/pipelines) |
| ralloc | [![Build status](https://gitlab.redox-os.org/redox-os/ralloc/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/ralloc/pipelines) |
| liner | [![Build status](https://gitlab.redox-os.org/redox-os/liner/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/liner/pipelines) |
| coreutils | ??? |
| orbclient | [![Build status](https://gitlab.redox-os.org/redox-os/orbclient/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/orbclient/pipelines) |
| tfs | [![Build status](https://gitlab.redox-os.org/redox-os/tfs/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/tfs/pipelines) |
| pkgutils | ??? |
| binutils | ??? |
| Orbital Terminal | [![Build status](https://gitlab.redox-os.org/redox-os/orbterm/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/orbterm/pipelines) |
| The Redox Book | ??? |
| redoxfs | [![Build status](https://gitlab.redox-os.org/redox-os/redoxfs/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/redoxfs/pipelines) |
| uutils | [![Build status](https://gitlab.redox-os.org/redox-os/uutils/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/uutils/pipelines) |
| OrbGame | [![Build status](https://gitlab.redox-os.org/redox-os/OrbGame/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/OrbGame/pipelines) |
| redox-ssh | [![Build status](https://gitlab.redox-os.org/redox-os/redox-ssh/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/redox-ssh/pipelines) |
| redoxer | ??? |
| drivers | ??? |
| installer | ??? |
| init | ??? |
| netutils | ??? |
| event | ??? |
| libc | ??? |
| seahash | ??? |
| rust-cairo | [![Build status](https://gitlab.redox-os.org/redox-os/rust-cairo/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/rust-cairo/pipelines) |
| reagent | [![Build status](https://gitlab.redox-os.org/redox-os/reagent/badges/master/build.svg)](https://gitlab.redox-os.org/redox-os/reagent/pipelines) |
| audiod | ??? |

